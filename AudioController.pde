class AudioController {
  SocialRecorder socialRecorder;
  AudioDatabase database;
  AudioInput audioInput;
  // AudioVisualizer audioVisualizer;

  AudioController() {
    audioInput = minim.getLineIn(Minim.MONO);
    // audioVisualizer = new AudioVisualizer();
    socialRecorder = new SocialRecorder();
    database = new AudioDatabase();
  }

  void run() {
    socialRecorder.run(audioInput, database);
    checkEvents();
    // simulateEvents();
    // audioVisualizer.visualize(audioInput);
  }
}
