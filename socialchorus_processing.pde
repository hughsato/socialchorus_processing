import ddf.minim.*;
import processing.net.*;

Client client;
Minim minim;
AudioController audioController;
EventStatus eventStatus = EventStatus.SIMULATION; // what was this for again?

final String SERVER_ADDRESS = "172.0.0.1",
  DATA_PATH = "/Users/hughsato/Sync/";

void settings() {
  size(400, 400);
}

void setup() {
  minim = new Minim(this);
  audioController = new AudioController();
  // client = (eventStatus == EventStatus.SERVER ? new Client(this, SERVER_ADDRESS, 5204) : null);
}

void draw() {
  background(0);
  audioController.run();
}
