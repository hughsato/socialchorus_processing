class AudioVisualizer {

  AudioVisualizer() {
  }

  void visualize(AudioInput audioInput) {
    for(int i = 0; i < audioInput.bufferSize() - 1; i++) {
      stroke(255);
      line(i, 50 + audioInput.left.get(i)*50, i+1, 50 + audioInput.left.get(i+1)*50);
      line(i, 150 + audioInput.right.get(i)*50, i+1, 150 + audioInput.right.get(i+1)*50);
    }
  }
}
