// requires fileTools.pde

class SocialRecorder {
  AudioRecorder recorder;

  boolean recording = false;
  boolean started = false;
  boolean finished = true;
  final String FILE_EXTENSION = ".wav";

  // key released
  boolean released = true;
  final char RECORDING_KEY = 'k';

  String filename = "";
  String timestamp = "";

  SocialRecorder() {
  }

  public void run(AudioInput audioInput, AudioDatabase audioDB) {
    checkButton();
    recordAudio(audioInput, audioDB);
  }

  private void recordAudio(AudioInput audioInput, AudioDatabase audioDB) {
    if (recording && !started) {
      started = true; finished = false;

      this.timestamp = getTimestamp();
      this.filename = this.timestamp + FILE_EXTENSION;

      createNewRecording(
        (DATA_PATH + filename), // from fileTools.pde
        audioInput
      );
    } else if (!finished && !recording) {
      finished = true; started = false;
      println("____________end of recording");

      this.endRecording(audioDB);
    }
  }

  private void checkButton() {
    if (keyPressed) {
      if (key == RECORDING_KEY) {
        recording = true;
        released = false;
      }
    } else if (!released) {
      released = true;
      recording = false;
    }
  }

  private void createNewRecording(String s, AudioInput audioInput) {
    recorder = minim.createRecorder(audioInput, s);
    recorder.beginRecord();
  }

  private void endRecording(AudioDatabase audioDB) {
    recorder.endRecord();
    recorder.save();
    audioDB.add(this.filename, this.timestamp);
  }
}
