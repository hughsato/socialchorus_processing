// a variety of functions for file handling

String getTimestamp() {
  String s = nf(month()) + "-" + nf(day()) + "_" + nf(hour()) + "-" + nf(minute()) + "-" + nf(second());
  return s;
}
