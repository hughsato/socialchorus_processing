import ddf.minim.*;

final int NUM_CHANNELS = 2;
Minim minim;
AudioDatabase audioDatabase;

// holds sample filenames to be loaded.
String[] sampleFilenames = new String[NUM_CHANNELS];

// holds the samples in memory
AudioSample[] samples = new AudioSample[NUM_CHANNELS];

// tells us whether they're ready to be used or not.
boolean[] activeSamples = new boolean[NUM_CHANNELS];

// sets the state in the playbackMachine.
int playbackState = 0;

void setup() {
  minim = new Minim(this);
  audioDatabase = new AudioDatabase();

  // select sample list a number of samples based on NUM_CHANNELS
  sampleFilenames = getSampleFilenames(audioDatabase.getStringArray(), NUM_CHANNELS);

  for (int i = 0; i < sampleFilenames.length; i ++) {
    println("got: " + sampleFilenames[i]);
  }

  // load initial samples
  // for (int i = 0; i < sampleFilenames.length; i++ ) {
  //   samples[i] = minim.loadSample(sampleFilenames[i]);
  //   if (samples[i] == null) println("Failed to load: " + i);
  // }

}

void draw() {


}

String[] getSampleFilenames(String[] sampleList, int selectLimit) {
  // based on selectLimit, create an array of random unique integers
  println("selectLimit: " + selectLimit + " sampleList: " + sampleList.length);
  int[] select = rndIndexArray(selectLimit, sampleList.length);
  println(select);
  // for each, pull in a corresponding array index filename to a new existsInArray
  for (int i = 0; i < sampleList.length; i++) {

  }

  // return that new array with filenames.
  return null;
}

void playbackMachine() {
  // the logic for running samples...
  // random, drunk sample trigger system...
  // keep one sample alive at all times with some delay in between.
  switch(playbackState) {
    case 0:
      // silence all
      break;
    case 1:
      // play only samples[0]
    case 2:
      // play only samples[0] + [1]
    case 3:
      // play samples [0], [1], [2]

      // when a sample loops, trigger a quick check to see if it randomly gets selected to be replaced.

  }

  // keep two samples alive at all times with some delay in between.

  // keep three samples looping, but randomly select one every 10 seconds to be changed out.

  // keep all channels filled and looping but select one every 10 seconds to be changed out.

}



int randomSelect(int maxIndex) {
  return (int) random(maxIndex);
}

void replaceSample(int sampleIndex, String filename) {

}
