int[] rndIndexArray(int arrSize, int limit) {
  if (arrSize > limit) {
      println("you want: " + arrSize + " files but have only " + limit + " files available ");
      return null;
  }

  int[] arr = new int[arrSize];
  // initialize array with negative num
  for (int i = 0; i < arrSize; i++) {
    arr[i] = -1;
  }

  for (int i = 0; i < arrSize; i++) {
      arr[i] = uniqueInt(arr, limit);
      println("picked: " + arr[i]);
  }
  return arr;
}

int uniqueInt(int[] array, int limit) {
  // get a random int
  int rndInt = (int) random(0, limit);
  // println("random: " + rndInt);

  // check if it exists inside the array already
  for (int i = 0; i < array.length; i++) {
    if(array[i] == rndInt) {
      return uniqueInt(array, limit);
    }
  }

  return rndInt;
}
