void logFoundKeyword(boolean b, String str) {
  if (b) {
    println("found: " + str);
  } else {
    println("not found: " + str);
  }
}

void logTableCount(int tableCount) {
  println("We now have " + tableCount + " files");
}

void logError(String error) {
  println(error);
}

void logMovieFileLoaded(String k, int whichItem){
  println("database loaded: " + k + whichItem);
}

void logMovieState(String state) {
  println(state);
}

void logRecordState(String state) {
  println("ready to record for: " + state);
}

void logEventName(String event) {
  println("got a control event from controller with name " + event);
}

void logLoaded(String item) {
  println("loaded: " + item);
}
