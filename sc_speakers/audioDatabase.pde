// class for handling anything to do with getting and logging files to the database csvTable
import java.io.File;

class AudioDatabase {
  Table csvTable;
  final int INIT_ARR_LENGTH = 0;

  AudioDatabase() {
    this.load();
    checkFileIntegrity();
  }

  void load() {
    // csvTable has headers filename,keyword
    csvTable = loadTable("audioDatabase.csv", "header");
    logLoaded("Audio Database: " + csvTable.getRowCount());
  }

  // does not need any adding functionality for sc_speakers

  String[] getStringArray() {
    // this would return all videos into the string[]
    String[] sr = new String[INIT_ARR_LENGTH];
    for (TableRow row : csvTable.rows()) {
      sr = append(sr, row.getString("filename"));
    }
    return sr;
  }

  String[] getStringArray(String timestamp) {
    // this returns only videos of a specific timestamp
    String[] sr = new String[INIT_ARR_LENGTH];
    for (TableRow row : csvTable.findRows(timestamp, "timestamp")) {
      // println("found " + row.getString("filename"));
      sr = append(sr, row.getString("filename"));
    }
    return sr;
  }

  void checkFileIntegrity() {
    int rowCount = csvTable.getRowCount();
    int[] removeArr = {};

    // by going from the length backward, we can maintain correct index
    // positions to remove form the array.
    for (int i = rowCount-1; i >= 0; i--) {
      TableRow row = csvTable.getRow(i);
      File f = new File( sketchPath("data/" + row.getString("filename")) );
      print("checking: " + row.getString("filename"));
      if (f.exists()) {
        print(" :ok");
      } else {
        print(" :removing");
        this.csvTable.removeRow(i);
      }
      println("");
    }

    saveTable(csvTable, "audioDatabase.csv");
  }
}
