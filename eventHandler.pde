int checkEvents() {
  if (eventStatus == EventStatus.SIMULATION) {
    simulateEvents();
  } else {
    if (client.available() > 0) {
      return client.read();
       // println(client.read());
    }
  }

  return 0;
}

// TODO: do a thread instead with a timeout if this is needed.
int simulateEvents() {
  if (millis() % 5000 == 0) {
    return(1);
  } else {
    return 0;
  }
}

enum EventStatus {
  SERVER,
  SIMULATION
  ;
}
