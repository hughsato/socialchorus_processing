/*

Some tests
----------

getDatedFilename should always return a string with the extension string provided.
nf(month()) + "-" + nf(day()) + "_" + nf(hour()) + "-" + nf(second()) + extension;

createDatedFilename(".wav") -> "*.wav"
createDatedFilename(".mov") -> "*.mov"
----------

socialRecorder functions besides Object.update() should be private and restrict access
----------

test csv structure that includes headers
----------

test that creating these classes always work and create expected variables
socialRecorder = new SocialRecorder();
database = new AudioDatabase();
----------

*/
